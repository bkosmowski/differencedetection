#include "../Header Files/HistogramsComparator.h"

HistogramsComparator::HistogramsComparator(cv::Mat wrapedReference, cv::Mat wrapedTested): pathDepth(0)
{
	this->referenceHistogramQuadrant = HistogramsQuadrant(wrapedReference);
	this->testedHistogramQuadrant = HistogramsQuadrant(wrapedTested);
}

HistogramsComparator::~HistogramsComparator()
{
}

cv::Mat HistogramsComparator::FindDifferencesBetweenImages()
{
	HistogramsQuadrant referenceQuadrant = this->referenceHistogramQuadrant;
	HistogramsQuadrant testedQuadrant = this->testedHistogramQuadrant;

	cv::Mat tempReference = this->referenceHistogramQuadrant.GetImage();
	cv::Mat tempTested = this->testedHistogramQuadrant.GetImage();

	cv::Mat selectedReferenceArea = referenceQuadrant.GetImage();
	cv::Mat selectedTestedArea = testedQuadrant.GetImage();

	AnalyzedImages analyzedImages;
	analyzedImages.ReferenceImage = this->referenceHistogramQuadrant.GetImage();
	analyzedImages.TestedImage = this->testedHistogramQuadrant.GetImage();

	this->pathDepth = FindPathDepth(analyzedImages.ReferenceImage.GetImage());

	std::vector<AnalyzedImages> dividedAnalyzedImages = DivideAnalyzedImages(analyzedImages);

	cv::Mat composedImage = cv::Mat::zeros(this->testedHistogramQuadrant.GetImage().rows, this->testedHistogramQuadrant.GetImage().cols, this->testedHistogramQuadrant.GetImage().type());

	for (int index = 0; index < dividedAnalyzedImages.size(); index++)
	{
		double lumaDifference = GetLumaDifference(dividedAnalyzedImages[index].ReferenceImage, dividedAnalyzedImages[index].TestedImage);

		double hueDifference = GetHueDifference(dividedAnalyzedImages[index].ReferenceImage, dividedAnalyzedImages[index].TestedImage);

		double saturationDifference = GetSaturationDifference(dividedAnalyzedImages[index].ReferenceImage, dividedAnalyzedImages[index].TestedImage);
		double mainThresh = 0.25;
		double helperThresh = 0.02;

		composedImage = ComposeImage(composedImage, saturationDifference > mainThresh && lumaDifference > helperThresh && hueDifference > helperThresh
			|| lumaDifference > mainThresh && saturationDifference > helperThresh && hueDifference > helperThresh
			|| hueDifference > mainThresh && saturationDifference > helperThresh && lumaDifference > helperThresh
			|| lumaDifference > 0.5 && saturationDifference > 0.1, index);
	}

	cv::cvtColor(composedImage, composedImage, CV_BGR2GRAY);

	return composedImage;
}

double HistogramsComparator::GetLumaDifference(ImageHistogram referenceHistogram, ImageHistogram testedHistogram)
{
	double lumaDifference = ImageProcessorHelper::CompareLumaHistograms(referenceHistogram.GetLumaHistogram(), testedHistogram.GetLumaHistogram());

	return lumaDifference;
}

double HistogramsComparator::GetHueDifference(ImageHistogram referenceHistogram, ImageHistogram testedHistogram)
{
	double hueDifference = ImageProcessorHelper::CompareHueHistograms(referenceHistogram.GetHSVHistogram(), testedHistogram.GetHSVHistogram());

	return hueDifference;
}

double HistogramsComparator::GetSaturationDifference(ImageHistogram referenceHistogram, ImageHistogram testedHistogram)
{
	double saturationDifference = ImageProcessorHelper::CompareSaturationHistograms(referenceHistogram.GetHSVHistogram(), testedHistogram.GetHSVHistogram());

	return saturationDifference;
}


cv::Mat HistogramsComparator::ComposeImage(cv::Mat image, bool isDifferent, int index) const
{
	int elementIndex = index;
	std::vector<int> searchingPath;
	int baseOfThePower = 4;
	for (int i = this->pathDepth - 1; i >= 0; i--)
	{
		int resultOfExponentiation = pow(baseOfThePower, i);
		int occurrencesNumber = 0;
		while ((elementIndex - resultOfExponentiation) >= 0)
		{
			occurrencesNumber++;
			elementIndex -= resultOfExponentiation;
		}
		searchingPath.push_back(occurrencesNumber);
	}

	DifferenceArea differenceArea = DifferenceArea(searchingPath, isDifferent,
		cv::Size(this->referenceHistogramQuadrant.GetImage().cols, this->referenceHistogramQuadrant.GetImage().rows),
		cv::Size(this->testedHistogramQuadrant.GetImage().cols, this->testedHistogramQuadrant.GetImage().rows));


	cv::Mat composedImage = differenceArea.CreateTestedImageWithSelectedArea(image);

	return composedImage;
}


std::vector<AnalyzedImages> HistogramsComparator::DivideAnalyzedImages(AnalyzedImages analyzedImages) const
{
	std::vector<AnalyzedImages> analyzedImagesLayer;

	analyzedImagesLayer.push_back(analyzedImages);

	for (int i = 0; i < this->pathDepth; i++)
	{
		std::vector<AnalyzedImages> analyzedImagesPreviousLayer = analyzedImagesLayer;

		analyzedImagesLayer.clear();

		for (int y = 0; y < analyzedImagesPreviousLayer.size(); y++)
		{
			HistogramsQuadrant referenceQuadrant = HistogramsQuadrant(analyzedImagesPreviousLayer[y].ReferenceImage.GetImage());
			HistogramsQuadrant testedQuadrant = HistogramsQuadrant(analyzedImagesPreviousLayer[y].TestedImage.GetImage());

			AnalyzedImages first;

			first.ReferenceImage = referenceQuadrant.GetFirstQuarterHistoram();
			first.TestedImage = testedQuadrant.GetFirstQuarterHistoram();
			analyzedImagesLayer.push_back(first);

			AnalyzedImages secod;

			secod.ReferenceImage = referenceQuadrant.GetSecondQuarterHistogram();
			secod.TestedImage = testedQuadrant.GetSecondQuarterHistogram();
			analyzedImagesLayer.push_back(secod);

			AnalyzedImages third;

			third.ReferenceImage = referenceQuadrant.GetThirdQuarterHistogram();
			third.TestedImage = testedQuadrant.GetThirdQuarterHistogram();
			analyzedImagesLayer.push_back(third);

			AnalyzedImages fourth;

			fourth.ReferenceImage = referenceQuadrant.GetFourthQuarterHistogram();
			fourth.TestedImage = testedQuadrant.GetFourthQuarterHistogram();
			analyzedImagesLayer.push_back(fourth);
		}
	}

	return analyzedImagesLayer;
}

int HistogramsComparator::FindPathDepth(cv::Mat image)
{
	std::vector<int> searchingPath;

	int smallerSize = std::min(image.cols, image.rows);

	while (smallerSize > 10 && smallerSize > (std::min(image.cols, image.rows) * 0.025))
	{
		smallerSize /= 2;
		searchingPath.push_back(1);
	}

	return searchingPath.size();
}
