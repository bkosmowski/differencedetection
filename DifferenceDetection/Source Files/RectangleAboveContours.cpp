#include "../Header Files/RectangleAboveContours.h"

RectangleAboveContours::RectangleAboveContours(std::vector<cv::Point> contours, cv::Mat contoursImage)
{
	this->contoursImage = contoursImage;
	this->middle = GetMiddle(contoursImage);
	this->averageForY = GetAverageYFromContours(contours);

	std::vector<cv::Point> firstHalf = GetFirstHalf(contours, this->averageForY);
	std::vector<cv::Point> secondHalf = GetSecondHalf(contours, this->averageForY);

	this->firstHalfAverageForX = GetAverageXFromContours(firstHalf);
	this->secondHalfAverageForX = GetAverageXFromContours(secondHalf);

	std::vector<cv::Point> firstQuarter = GetFirstQuarter(firstHalf, firstHalfAverageForX);
	std::vector<cv::Point> secondQuarter = GetSecondQuarter(firstHalf, firstHalfAverageForX);
	std::vector<cv::Point> thirdQuarter = GetThirdQuarter(secondHalf, secondHalfAverageForX);
	std::vector<cv::Point> fourthQuarter = GetFourthQuarter(secondHalf, secondHalfAverageForX);

	SetPotentialsPointsForFirstCorner(firstQuarter);
	SetPotentialsPointForSecondCorner(secondQuarter, contoursImage.cols);
	SetPotentialsPointForThirdCorner(thirdQuarter, contoursImage.rows);
	SetPotentialPointsForFourthCorner(fourthQuarter, contoursImage.cols, contoursImage.rows);

	this->potentialPointsForFirstCorner.push_back(GetMinXPoint(contours));
	this->potentialPointsForFirstCorner.push_back(GetMinYPoint(contours));

	this->potentialPointsForSecondCorner.push_back(GetMinYPoint(contours));
	this->potentialPointsForSecondCorner.push_back(GetMaxXPoint(contours));

	this->potentialPointsForThirdCorner.push_back(GetMaxYPoint(contours));
	this->potentialPointsForThirdCorner.push_back(GetMinXPoint(contours));

	this->potentialPointsForFourthCorner.push_back(GetMaxXPoint(contours));
	this->potentialPointsForFourthCorner.push_back(GetMaxYPoint(contours));

	SetCornersForMaxArea();
}

RectangleAboveContours::~RectangleAboveContours()
{
}

cv::Point2f RectangleAboveContours::GetFirstCorner() const
{
	return this->firstCorner;
}

cv::Point2f RectangleAboveContours::GetSecondCorner() const
{
	return this->secondCorner;
}

cv::Point2f RectangleAboveContours::GetThirdCorner() const
{
	return this->thirdCorner;
}

cv::Point2f RectangleAboveContours::GetFourthCorner() const
{
	return this->fourthCorner;
}

double RectangleAboveContours::GetAverageFirstX() const
{
	return this->firstHalfAverageForX;
}

double RectangleAboveContours::GetAverageSecondX() const
{
	return this->secondHalfAverageForX;
}

double RectangleAboveContours::GetAverageY() const
{
	return this->averageForY;
}

cv::Point2f RectangleAboveContours::GetMiddle(cv::Mat mat)
{
	double x = mat.cols / 2.0;
	double y = mat.rows / 2.0;

	return cv::Point2f(x, y);
}

std::vector<cv::Point> RectangleAboveContours::GetFirstHalf(std::vector<cv::Point> contours, double averageForY)
{
	std::vector<cv::Point> firstHalf;
	for (int i = 0; i < contours.size(); i++)
	{
		if (contours[i].y < averageForY)
		{
			firstHalf.push_back(contours[i]);
		}
	}

	return firstHalf;
}

std::vector<cv::Point> RectangleAboveContours::GetSecondHalf(std::vector<cv::Point> contours, double averageForY)
{
	std::vector<cv::Point> secondHalf;
	for (int i = 0; i < contours.size(); i++)
	{
		if (contours[i].y >= averageForY)
		{
			secondHalf.push_back(contours[i]);
		}
	}

	return secondHalf;
}

std::vector<cv::Point> RectangleAboveContours::GetFirstQuarter(std::vector<cv::Point> firstHalf, double averageForX)
{
	std::vector<cv::Point> firstQuarter = GetSmallerPoints(firstHalf, averageForX);

	return firstQuarter;
}

std::vector<cv::Point> RectangleAboveContours::GetSecondQuarter(std::vector<cv::Point> firstHalf, double averageForX)
{
	std::vector<cv::Point> secondQuarter = GetLargerOrEqualPoints(firstHalf, averageForX);

	return secondQuarter;
}

std::vector<cv::Point> RectangleAboveContours::GetThirdQuarter(std::vector<cv::Point> secondHalf, double averageForX)
{
	std::vector<cv::Point> thirdQuarter = GetSmallerPoints(secondHalf, averageForX);

	return thirdQuarter;
}

std::vector<cv::Point> RectangleAboveContours::GetFourthQuarter(std::vector<cv::Point> secondHalf, double averageForX)
{
	std::vector<cv::Point> fourthQuarter = GetLargerOrEqualPoints(secondHalf, averageForX);

	return fourthQuarter;
}

cv::Point RectangleAboveContours::GetMinXPoint(std::vector<cv::Point> points)
{
	cv::Point minX = points[0];
	for (int i = 0; i < points.size(); i++)
	{
		if (points[i].x < minX.x)
		{
			minX = points[i];
		}
	}
	return minX;
}

cv::Point RectangleAboveContours::GetMaxXPoint(std::vector<cv::Point> points)
{
	cv::Point maxX = points[0];
	for (int i = 0; i < points.size(); i++)
	{
		if (points[i].x > maxX.x)
		{
			maxX = points[i];
		}
	}
	return maxX;
}

cv::Point RectangleAboveContours::GetMinYPoint(std::vector<cv::Point> points)
{
	cv::Point minX = points[0];
	for (int i = 0; i < points.size(); i++)
	{
		if (points[i].y < minX.y)
		{
			minX = points[i];
		}
	}
	return minX;
}

cv::Point RectangleAboveContours::GetMaxYPoint(std::vector<cv::Point> points)
{
	cv::Point minX = points[0];
	for (int i = 0; i < points.size(); i++)
	{
		if (points[i].y > minX.y)
		{
			minX = points[i];
		}
	}
	return minX;
}

std::vector<cv::Point> RectangleAboveContours::GetSmallerPoints(std::vector<cv::Point> points, double averageForX)
{
	std::vector<cv::Point> smallerPoints;
	for (int i = 0; i < points.size(); i++)
	{
		if (points[i].x < averageForX)
		{
			smallerPoints.push_back(points[i]);
		}
	}

	return smallerPoints;
}

std::vector<cv::Point> RectangleAboveContours::GetLargerOrEqualPoints(std::vector<cv::Point> secondHalf, double averageForX)
{
	std::vector<cv::Point> largerOrEqualPoints;

	for (int i = 0; i < secondHalf.size(); i++)
	{
		if (secondHalf[i].x >= averageForX)
		{
			largerOrEqualPoints.push_back(secondHalf[i]);
		}
	}

	return largerOrEqualPoints;
}

double RectangleAboveContours::GetAverageXFromContours(std::vector<cv::Point> contours)
{
	double average = 0.0;

	for (int i = 0; i < contours.size(); i++)
	{
		average += contours[i].x;
	}

	average /= contours.size();

	return average;
}

double RectangleAboveContours::GetAverageYFromContours(std::vector<cv::Point> contours)
{
	double average = 0.0;

	for (int i = 0; i < contours.size(); i++)
	{
		average += contours[i].y;
	}

	average /= contours.size();

	return average;
}

double RectangleAboveContours::CalculateAngle(cv::Point firstPoint, cv::Point secondPoint, cv::Point thirdPoint) const
{
	double d12 = ImageProcessorHelper().CalculateEuclideanDistance(firstPoint, secondPoint);
	double d13 = ImageProcessorHelper().CalculateEuclideanDistance(firstPoint, thirdPoint);
	double d23 = ImageProcessorHelper().CalculateEuclideanDistance(secondPoint, thirdPoint);
	double theta = acos((d12*d12 + d13*d13 - d23*d23) / (2 * d12 * d13)) * (180.0 / CV_PI);

	return theta;
}
void RectangleAboveContours::SetPotentialsPointsForFirstCorner(std::vector<cv::Point> contours)
{
	double distanceToMiddle = 0.0;
	cv::Point2f cornerWithMaxDistanceToMiddle = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	double minEuclideanDistance = std::numeric_limits<double>::max();
	cv::Point2f potentialCorner = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	cv::Point2f pointWithMinDistanceToX = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	cv::Point2f pointWithMinDistanceToY = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	cv::Point2f referencePoint = cv::Point2f(0, 0);
	for (int i = 0; i < contours.size(); i++)
	{
		double currentEuclideaDistance = sqrt(pow(contours[i].x - referencePoint.x, 2)
			+ pow(contours[i].y - referencePoint.y, 2));

		double currentDistanceToMiddle = sqrt(pow(contours[i].x - this->middle.x, 2)
			+ pow(contours[i].y - this->middle.y, 2));

		if (distanceToMiddle < currentDistanceToMiddle)
		{
			cornerWithMaxDistanceToMiddle = contours[i];
			distanceToMiddle = currentDistanceToMiddle;
		}

		if (currentEuclideaDistance < minEuclideanDistance)
		{
			potentialCorner = contours[i];
			minEuclideanDistance = currentEuclideaDistance;
		}

		if (pointWithMinDistanceToX.x > contours[i].x)
		{
			pointWithMinDistanceToX = contours[i];
		}

		if (pointWithMinDistanceToY.y > contours[i].y)
		{
			pointWithMinDistanceToY = contours[i];
		}
	}

	this->potentialPointsForFirstCorner.push_back(potentialCorner);
	this->potentialPointsForFirstCorner.push_back(cornerWithMaxDistanceToMiddle);
	this->potentialPointsForFirstCorner.push_back(pointWithMinDistanceToX);
	this->potentialPointsForFirstCorner.push_back(pointWithMinDistanceToY);
}

void RectangleAboveContours::SetPotentialsPointForSecondCorner(std::vector<cv::Point> contours, int matWidth)
{
	double distanceToMiddle = 0.0;
	cv::Point2f cornerWithMaxDistanceToMiddle = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	double minEuclideanDistance = std::numeric_limits<double>::max();
	cv::Point2f potentialCorner = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	cv::Point2f pointWithMinDistanceToX = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	cv::Point2f pointWithMinDistanceToY = cv::Point2f(std::numeric_limits<float>::min(), std::numeric_limits<float>::max());

	cv::Point2f referencePoint = cv::Point2f(matWidth, 0);
	for (int i = 0; i < contours.size(); i++)
	{
		double currentEuclideaDistance = sqrt(pow(contours[i].x - referencePoint.x, 2)
			+ pow(contours[i].y - referencePoint.y, 2));

		double currentDistanceToMiddle = sqrt(pow(contours[i].x - this->middle.x, 2)
			+ pow(contours[i].y - this->middle.y, 2));

		if (distanceToMiddle < currentDistanceToMiddle)
		{
			cornerWithMaxDistanceToMiddle = contours[i];
			distanceToMiddle = currentDistanceToMiddle;
		}

		if (currentEuclideaDistance < minEuclideanDistance)
		{
			potentialCorner = contours[i];
			minEuclideanDistance = currentEuclideaDistance;
		}

		if (abs(matWidth - pointWithMinDistanceToX.x) > abs(matWidth - contours[i].x))
		{
			pointWithMinDistanceToX = contours[i];
		}

		if (pointWithMinDistanceToY.y > contours[i].y)
		{
			pointWithMinDistanceToY = contours[i];
		}
	}

	this->potentialPointsForSecondCorner.push_back(potentialCorner);
	this->potentialPointsForSecondCorner.push_back(cornerWithMaxDistanceToMiddle);

	this->potentialPointsForSecondCorner.push_back(pointWithMinDistanceToX);
	this->potentialPointsForSecondCorner.push_back(pointWithMinDistanceToY);
}

void RectangleAboveContours::SetPotentialsPointForThirdCorner(std::vector<cv::Point> contours, int matHeight)
{
	double distanceToMiddle = 0.0;
	cv::Point2f cornerWithMaxDistanceToMiddle = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	double minEuclideanDistance = std::numeric_limits<double>::max();
	cv::Point2f potentialCorner = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::min());

	cv::Point2f pointWithMinDistanceToX = cv::Point2f(std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	cv::Point2f pointWithMinDistanceToY = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::min());

	cv::Point2f referencePoint = cv::Point2f(0, matHeight);
	for (int i = 0; i < contours.size(); i++)
	{
		double currentEuclideaDistance = sqrt(pow(contours[i].x - referencePoint.x, 2)
			+ pow(contours[i].y - referencePoint.y, 2));

		double currentDistanceToMiddle = sqrt(pow(contours[i].x - this->middle.x, 2)
			+ pow(contours[i].y - this->middle.y, 2));

		if (distanceToMiddle < currentDistanceToMiddle)
		{
			cornerWithMaxDistanceToMiddle = contours[i];
			distanceToMiddle = currentDistanceToMiddle;
		}

		if (currentEuclideaDistance < minEuclideanDistance)
		{
			potentialCorner = contours[i];
			minEuclideanDistance = currentEuclideaDistance;
		}

		if (pointWithMinDistanceToX.x < contours[i].x)
		{
			pointWithMinDistanceToX = contours[i];
		}

		if (abs(pointWithMinDistanceToY.y - matHeight) > abs(contours[i].y - matHeight))
		{
			pointWithMinDistanceToY = contours[i];
		}
	}

	this->potentialPointsForThirdCorner.push_back(potentialCorner);
	this->potentialPointsForThirdCorner.push_back(cornerWithMaxDistanceToMiddle);

	this->potentialPointsForThirdCorner.push_back(pointWithMinDistanceToX);
	this->potentialPointsForThirdCorner.push_back(pointWithMinDistanceToY);
}

void RectangleAboveContours::SetPotentialPointsForFourthCorner(std::vector<cv::Point> contours, int matWidth, int matHeight)
{
	double distanceToMiddle = 0.0;
	cv::Point2f cornerWithMaxDistanceToMiddle = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	double minEuclideanDistance = std::numeric_limits<double>::max();
	cv::Point2f potentialCorner = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	cv::Point2f pointWithMinDistanceToX = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	cv::Point2f pointWithMinDistanceToY = cv::Point2f(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

	cv::Point2f referencePoint = cv::Point2f(matWidth, matHeight);
	for (int i = 0; i < contours.size(); i++)
	{
		double currentEuclideaDistance = sqrt(pow(contours[i].x - referencePoint.x, 2)
			+ pow(contours[i].y - referencePoint.y, 2));

		double currentDistanceToMiddle = sqrt(pow(contours[i].x - this->middle.x, 2)
			+ pow(contours[i].y - this->middle.y, 2));

		if (distanceToMiddle < currentDistanceToMiddle)
		{
			cornerWithMaxDistanceToMiddle = contours[i];
			distanceToMiddle = currentDistanceToMiddle;
		}

		if (currentEuclideaDistance < minEuclideanDistance)
		{
			potentialCorner = contours[i];
			minEuclideanDistance = currentEuclideaDistance;
		}

		if (abs(matWidth - pointWithMinDistanceToX.x) > abs(matWidth - contours[i].x))
		{
			pointWithMinDistanceToX = contours[i];
		}

		if (abs(pointWithMinDistanceToY.y - matHeight) > abs(contours[i].y - matHeight))
		{
			pointWithMinDistanceToY = contours[i];
		}
	}

	this->potentialPointsForFourthCorner.push_back(potentialCorner);
	this->potentialPointsForFourthCorner.push_back(cornerWithMaxDistanceToMiddle);

	this->potentialPointsForFourthCorner.push_back(pointWithMinDistanceToX);
	this->potentialPointsForFourthCorner.push_back(pointWithMinDistanceToY);
}

void RectangleAboveContours::SetCornersForMaxArea()
{
	double maxArea = 0;

	cv::Point firstPotentialCorner = this->potentialPointsForFirstCorner[0];
	cv::Point secondPotentialCorner = this->potentialPointsForSecondCorner[0];
	cv::Point thirdPotentialCorner = this->potentialPointsForThirdCorner[0];
	cv::Point fourthPotentialCorner = this->potentialPointsForFourthCorner[0];

	for (int firstIndex = 0; firstIndex < this->potentialPointsForFirstCorner.size(); firstIndex++)
	{
		for (int secondIndex = 0; secondIndex < this->potentialPointsForSecondCorner.size(); secondIndex++)
		{
			for (int thirdIndex = 0; thirdIndex < this->potentialPointsForThirdCorner.size(); thirdIndex++)
			{
				for (int fourthIndex = 0; fourthIndex < this->potentialPointsForFourthCorner.size(); fourthIndex++)
				{
					double currentFirstSideLength = ImageProcessorHelper().CalculateEuclideanDistance(this->potentialPointsForFirstCorner[firstIndex],
						this->potentialPointsForSecondCorner[secondIndex]);
					double currentSecondSideLength = ImageProcessorHelper().CalculateEuclideanDistance(this->potentialPointsForSecondCorner[secondIndex],
						this->potentialPointsForFourthCorner[fourthIndex]);
					double currentThirdSideLength = ImageProcessorHelper().CalculateEuclideanDistance(this->potentialPointsForFourthCorner[fourthIndex],
						this->potentialPointsForThirdCorner[thirdIndex]);
					double currentFourthSideLength = ImageProcessorHelper().CalculateEuclideanDistance(this->potentialPointsForThirdCorner[thirdIndex],
						this->potentialPointsForFirstCorner[firstIndex]);

					double currentArea = CaulculateArea(this->potentialPointsForFirstCorner[firstIndex], this->potentialPointsForSecondCorner[secondIndex],
						this->potentialPointsForThirdCorner[thirdIndex], this->potentialPointsForFourthCorner[fourthIndex]);

					if (maxArea < currentArea)
					{
						maxArea = currentArea;
						firstPotentialCorner = this->potentialPointsForFirstCorner[firstIndex];
						secondPotentialCorner = this->potentialPointsForSecondCorner[secondIndex];
						thirdPotentialCorner = this->potentialPointsForThirdCorner[thirdIndex];
						fourthPotentialCorner = this->potentialPointsForFourthCorner[fourthIndex];
						this->firstSideLength = currentFirstSideLength;
						this->secondSideLength = currentSecondSideLength;
						this->thirdSideLength = currentThirdSideLength;
						this->fourthSideLength = currentFourthSideLength;
					}

				}
			}
		}
	}

	SetPotentialCorners(firstPotentialCorner, secondPotentialCorner, thirdPotentialCorner, fourthPotentialCorner);
}

double RectangleAboveContours::CaulculateArea(cv::Point firstPoint, cv::Point secondPoint, cv::Point thirdPoint, cv::Point fourthPoint) const
{
	double area = abs(secondPoint.y - thirdPoint.y) * abs(firstPoint.x - fourthPoint.x)
		- abs(firstPoint.x - secondPoint.x) * abs(firstPoint.y - secondPoint.y) / 2.0
		- abs(firstPoint.x - thirdPoint.x) * abs(firstPoint.y - thirdPoint.y) / 2.0
		- abs(thirdPoint.x - fourthPoint.x) * abs(thirdPoint.y - fourthPoint.y) / 2.0
		- abs(secondPoint.x - fourthPoint.x) * abs(secondPoint.y - fourthPoint.y) / 2.0;

	return area;
}

void RectangleAboveContours::SetPotentialCorners(cv::Point firstPotentialCorner, cv::Point secondPotentialCorner,
	cv::Point thirdPotentialCorner, cv::Point fourthPotentialCorner)

{
	this->firstCorner = firstPotentialCorner;
	this->secondCorner = secondPotentialCorner;
	this->thirdCorner = thirdPotentialCorner;
	this->fourthCorner = fourthPotentialCorner;
}