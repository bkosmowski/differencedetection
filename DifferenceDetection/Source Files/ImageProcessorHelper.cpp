#include "../Header Files/ImageProcessorHelper.h"

ImageProcessorHelper::ImageProcessorHelper()
{
}

ImageProcessorHelper::~ImageProcessorHelper()
{
}

HSVHistogram ImageProcessorHelper::CalculateHSVHistogram(cv::Mat image)
{
	HSVHistogram histogram;
	cv::Mat hsvImage;
	cvtColor(image, hsvImage, CV_BGR2HSV);

	for (int x = 0; x < hsvImage.cols; x++)
	{
		for (int y = 0; y < hsvImage.rows; y++)
		{
			cv::Vec3b color = hsvImage.at<cv::Vec3b>(y, x);

			int hueIndex = static_cast<int>(color.val[0]);
			int saturationIndex = static_cast<int>(color.val[1]);

			histogram.HueBuckets[hueIndex]++;
			histogram.SaturationBuckets[saturationIndex]++;
		}
	}

	return histogram;
}

double ImageProcessorHelper::CompareHueHistograms(HSVHistogram referenceHistogram, HSVHistogram testedHistogram)
{
	double referenceMaxValue = 0.0;
	double testedMaxValue = 0.0;

	double referenceElemnetsSum = 0.0;
	double testedElemnetsSum = 0.0;

	double referenceHueSum = 0.0;
	double testedHueSum = 0.0;

	for (int index = 0; index < sizeof(referenceHistogram.HueBuckets) / sizeof(*referenceHistogram.HueBuckets); index++)
	{
		if (referenceMaxValue < referenceHistogram.HueBuckets[index])
		{
			referenceMaxValue = referenceHistogram.HueBuckets[index];
		}
		if (testedMaxValue < testedHistogram.HueBuckets[index])
		{
			testedMaxValue = testedHistogram.HueBuckets[index];
		}
	}

	for (int index = 0; index < sizeof(referenceHistogram.HueBuckets) / sizeof(*referenceHistogram.HueBuckets); index++)
	{
		if (0.8 * referenceMaxValue < referenceHistogram.HueBuckets[index])
		{
			referenceElemnetsSum += referenceHistogram.HueBuckets[index];
			referenceHueSum += referenceHistogram.HueBuckets[index] * index;
		}
		if (0.8 * testedMaxValue < testedHistogram.HueBuckets[index])
		{
			testedElemnetsSum += testedHistogram.HueBuckets[index];
			testedHueSum += testedHistogram.HueBuckets[index] * index;
		}
	}

	return abs(referenceHueSum / referenceElemnetsSum - testedHueSum / testedElemnetsSum) / 180;
}

double ImageProcessorHelper::CompareSaturationHistograms(HSVHistogram referenceHistogram, HSVHistogram testedHistogram)
{
	double referenceMaxValue = 0.0;
	double testedMaxValue = 0.0;

	double referenceElemnetsSum = 0.0;
	double testedElemnetsSum = 0.0;

	double referenceSaturationSum = 0.0;
	double testedSaturationSum = 0.0;

	for (int index = 0; index < sizeof(referenceHistogram.SaturationBuckets) / sizeof(*referenceHistogram.SaturationBuckets); index++)
	{
		if (referenceMaxValue < referenceHistogram.SaturationBuckets[index])
		{
			referenceMaxValue = referenceHistogram.SaturationBuckets[index];
		}
		if (testedMaxValue < testedHistogram.SaturationBuckets[index])
		{
			testedMaxValue = testedHistogram.SaturationBuckets[index];
		}
	}

	for (int index = 0; index < sizeof(referenceHistogram.SaturationBuckets) / sizeof(*referenceHistogram.SaturationBuckets); index++)
	{
		if (0.8 * referenceMaxValue < referenceHistogram.SaturationBuckets[index])
		{
			referenceElemnetsSum += referenceHistogram.SaturationBuckets[index];
			referenceSaturationSum += referenceHistogram.SaturationBuckets[index] * index;
		}
		if (0.8 * testedMaxValue < testedHistogram.SaturationBuckets[index])
		{
			testedElemnetsSum += testedHistogram.SaturationBuckets[index];
			testedSaturationSum += testedHistogram.SaturationBuckets[index] * index;
		}
	}

	return abs(referenceSaturationSum / referenceElemnetsSum - testedSaturationSum / testedElemnetsSum) / 180;
}

LumaHistogram ImageProcessorHelper::CalculateLumaHistogram(cv::Mat image)
{
	LumaHistogram histogram;
	for (int x = 0; x < image.cols; x++)
	{
		for (int y = 0; y < image.rows; y++)
		{
			cv::Vec3b color = image.at<cv::Vec3b>(cv::Point(x, y));

			int blue = static_cast<int>(color.val[0]);
			int green = static_cast<int>(color.val[1]);
			int red = static_cast<int>(color.val[2]);

			double luma = CalculateLuma(blue, green, red);

			int bucketIndex = GetBucketForLuma(luma);

			histogram.Buckets[bucketIndex]++;
		}
	}

	return histogram;
}

double ImageProcessorHelper::CalculateLuma(int blue, int green, int red)
{
	double luma = 0.2126 * red + 0.7152 * green + 0.0722 * blue;

	return luma;
}

int ImageProcessorHelper::GetBucketForLuma(double luma)
{
	int bucketIndex = luma;

	return bucketIndex;
}

double ImageProcessorHelper::CompareLumaHistograms(LumaHistogram referenceHistogram, LumaHistogram testedHistogram)
{
	double referenceMaxValue = 0.0;
	double testedMaxValue = 0.0;

	double referenceElemnetsSum = 0.0;
	double testedElemnetsSum = 0.0;

	double referenceColorSum = 0.0;
	double testedColorSum = 0.0;

	for (int index = 0; index < sizeof(referenceHistogram.Buckets) / sizeof(*referenceHistogram.Buckets); index++)
	{
		if (referenceMaxValue < referenceHistogram.Buckets[index])
		{
			referenceMaxValue = referenceHistogram.Buckets[index];
		}
		if (testedMaxValue < testedHistogram.Buckets[index])
		{
			testedMaxValue = testedHistogram.Buckets[index];
		}
	}

	for (int index = 0; index < sizeof(referenceHistogram.Buckets) / sizeof(*referenceHistogram.Buckets); index++)
	{
		if (0.8 * referenceMaxValue < referenceHistogram.Buckets[index])
		{
			referenceElemnetsSum += referenceHistogram.Buckets[index];
			referenceColorSum += referenceHistogram.Buckets[index] * index;
		}
		if (0.8 * testedMaxValue < testedHistogram.Buckets[index])
		{
			testedElemnetsSum += testedHistogram.Buckets[index];
			testedColorSum += testedHistogram.Buckets[index] * index;
		}
	}

	return abs(referenceColorSum / referenceElemnetsSum - testedColorSum / testedElemnetsSum) / 256;
}

double ImageProcessorHelper::CalculateEuclideanDistance(cv::Point firstPoint, cv::Point secondPoint)
{
	double euclideanDistance = sqrt(pow(firstPoint.x - secondPoint.x, 2)
		+ pow(firstPoint.y - secondPoint.y, 2));

	return euclideanDistance;
}

cv::Mat ImageProcessorHelper::FourPointTransform(std::vector<cv::Point2f> corners, cv::Mat sourceImage)
{
	std::vector<cv::Point2f> src;
	double firstSideLength = CalculateEuclideanDistance(corners[0], corners[1]);
	double secondSideLength = CalculateEuclideanDistance(corners[1], corners[2]);
	double thirdSideLength = CalculateEuclideanDistance(corners[2], corners[3]);
	double fourthSideLength = CalculateEuclideanDistance(corners[3], corners[0]);

	int maxWidth = std::max(firstSideLength, thirdSideLength);
	int maxHeight = std::max(secondSideLength, fourthSideLength);

	std::vector<cv::Point2f> dst;
	dst.push_back(cv::Point2f(0, 0));
	dst.push_back(cv::Point2f(maxWidth - 1, 0));
	dst.push_back(cv::Point2f(maxWidth - 1, maxHeight - 1));
	dst.push_back(cv::Point2f(0, maxHeight - 1));

	cv::Mat M = cv::getPerspectiveTransform(corners, dst);

	cv::Mat wraped;
	cv::warpPerspective(sourceImage, wraped, M, cv::Size(maxWidth, maxHeight));

	return wraped;
}