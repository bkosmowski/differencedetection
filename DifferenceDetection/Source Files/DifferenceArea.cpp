#include "../Header Files/DifferenceArea.h"

DifferenceArea::DifferenceArea(std::vector<int> searchingPath, bool isDifference, cv::Size referenceImageSize, cv::Size testedImageSize)
{
	this->searchingPath = searchingPath;
	this->isDifference = isDifference;
	this->referenceRectangle = CreateRectangleFromPath(referenceImageSize);
	this->testedRectangle = CreateRectangleFromPath(testedImageSize);
}

DifferenceArea::~DifferenceArea()
{
}


cv::Mat DifferenceArea::CreateTestedImageWithSelectedArea(cv::Mat testedImage) const
{
	cv::Mat testedImageWithSelectedArea = testedImage.clone();

	cv::Scalar white = cv::Scalar(255, 255, 255);
	cv::Scalar black = cv::Scalar(0, 0, 0);
	cv::Scalar color = this->isDifference ? white : black;

	for (int x = this->testedRectangle.x; x < this->testedRectangle.x + this->testedRectangle.width && x < testedImage.cols; x++)
	{
		for (int y = this->testedRectangle.y; y < this->testedRectangle.y + this->testedRectangle.height && y < testedImage.rows; y++)
		{
				testedImageWithSelectedArea.at<cv::Vec3b>(cv::Point(x, y))[0] = color[0];
				testedImageWithSelectedArea.at<cv::Vec3b>(cv::Point(x, y))[1] = color[1];
				testedImageWithSelectedArea.at<cv::Vec3b>(cv::Point(x, y))[2] = color[2];
 		}
	}

	return testedImageWithSelectedArea;
}

cv::Rect DifferenceArea::CreateRectangleFromPath(cv::Size imageSize) const
{
	double x = 0;
	double y = 0;
	double width = imageSize.width;
	double height = imageSize.height;
	for (int index = 0; index < searchingPath.size(); index++)
	{
		if (searchingPath[index] == 0)
		{
			x += 0;
			y += 0;
		}
		else if (searchingPath[index] == 1)
		{
			x += width / 2;
			y += 0;
		}
		else if (searchingPath[index] == 2)
		{
			x += 0;
			y += height / 2;
		}
		else
		{
			x += width / 2;
			y += height / 2;
		}

		width = width / 2;
		height = height / 2;
	}
	if (x > 0)
	{
		x -= 1;
	}
	if (y > 0)
	{
		y -= 1;
	}

	cv::Rect selectedArea = cv::Rect(floor(x), floor(y), ceil(width), ceil(height));

	return selectedArea;
}