#include "../Header Files/Source.h"
void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);

	horizontal = desktop.right;
	vertical = desktop.bottom;
}

bool compareContourAreas(std::vector<cv::Point> firstContour, std::vector<cv::Point> secondContour) {
	double firstLength = arcLength(firstContour, false);
	double secondLength = arcLength(secondContour, false);
	return firstLength > secondLength;
}

int main(int argc, char** argv)
{
	cv::Mat imgReferenceBase = cv::imread("Pictures/Last test/IMG_1505.JPG");
	cv::Mat imgTestedBase = cv::imread("Pictures/Last test/IMG_1506.JPG");

	//http://stackoverflow.com/questions/15043152/rotate-opencv-matrix-by-90-180-270-degrees

	cv::Mat imgReferenceClone = imgReferenceBase.clone();
	cv::Mat imgTestedClone = imgTestedBase.clone();


	cvtColor(imgReferenceBase, imgReferenceBase, CV_BGR2GRAY);
	cvtColor(imgTestedBase, imgTestedBase, CV_BGR2GRAY);

	int horizontal = 0;
	int vertical = 0;
	GetDesktopResolution(horizontal, vertical);

	if (!imgReferenceBase.data || !imgTestedBase.data)
	{
		std::cout << " --(!) Error reading images " << std::endl; return -1;
	}

	cv::Size referenceImageSize(imgReferenceBase.cols / (ceil(imgReferenceBase.cols * 1.1 / horizontal)), imgReferenceBase.rows / (ceil(imgReferenceBase.rows * 1.1 / vertical)));
	cv::Size testedImageSize(imgTestedBase.cols / (ceil(imgTestedBase.cols * 1.1 / horizontal)), imgTestedBase.rows / (ceil(imgTestedBase.rows * 1.1 / vertical)));

	resize(imgReferenceClone, imgReferenceClone, referenceImageSize, 0, 0);
	resize(imgTestedClone, imgTestedClone, testedImageSize, 0, 0);

	std::vector<std::vector<cv::Point>> contoursTested;
	std::vector<cv::Vec4i> hierarchyTested;
	cv::Mat cannyTested;

	cv::Mat contoursSourceTestedImg = imgTestedClone.clone();

	cvtColor(contoursSourceTestedImg, contoursSourceTestedImg, CV_BGR2GRAY);

	threshold(contoursSourceTestedImg, contoursSourceTestedImg, 67, 255, cv::THRESH_TOZERO);

	cv::Mat testedGradX;
	Sobel(contoursSourceTestedImg, testedGradX, CV_16S, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
	cv::Mat absTestedGradX;
	convertScaleAbs(testedGradX, absTestedGradX);

	cv::Mat testedGradY;
	Sobel(contoursSourceTestedImg, testedGradY, CV_16S, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
	cv::Mat absTestedGradY;
	convertScaleAbs(testedGradY, absTestedGradY);

	cv::Mat testedSobelResult;
	addWeighted(absTestedGradX, 0.5, absTestedGradY, 0.5, 0, testedSobelResult);
	
	GaussianBlur(testedSobelResult, testedSobelResult, cv::Size(11, 11), 0, 0, cv::BORDER_DEFAULT);

	Canny(testedSobelResult, cannyTested, 75, 200);

	findContours(cannyTested, contoursTested, hierarchyTested, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	cv::Mat drawingTested = cv::Mat::zeros(testedSobelResult.size(), CV_8UC3);
	
	std::sort(contoursTested.begin(), contoursTested.end(), compareContourAreas);
	
	std::vector<cv::Point> selectedVectorOfTestedContours; 

	for (int index = 0; index < contoursTested.size() && arcLength(contoursTested[index], false) > arcLength(contoursTested[0], false) * 0.01; index++)
	{
		selectedVectorOfTestedContours.insert(selectedVectorOfTestedContours.end(), contoursTested[index].begin(), contoursTested[index].end());
		drawContours(drawingTested, contoursTested, index, cv::Scalar(255, 255, 255), CV_FILLED, 8, hierarchyTested);
	}

	RectangleAboveContours rectangleAboveTested = RectangleAboveContours(selectedVectorOfTestedContours, drawingTested);

	cv::Mat testedImageSource = imgTestedClone.clone();

	std::vector<cv::Point2f> testedCorners;
	testedCorners.push_back(rectangleAboveTested.GetFirstCorner());
	testedCorners.push_back(rectangleAboveTested.GetSecondCorner());
	testedCorners.push_back(rectangleAboveTested.GetFourthCorner());
	testedCorners.push_back(rectangleAboveTested.GetThirdCorner());

	cv::Mat wrapedTested = ImageProcessorHelper().FourPointTransform(testedCorners, testedImageSource);

	line(imgTestedClone, rectangleAboveTested.GetFirstCorner(), rectangleAboveTested.GetSecondCorner(), cv::Scalar(0, 255, 0), 2, 8);
	line(imgTestedClone, rectangleAboveTested.GetSecondCorner(), rectangleAboveTested.GetFourthCorner(), cv::Scalar(0, 255, 0), 2, 8);
	line(imgTestedClone, rectangleAboveTested.GetFourthCorner(), rectangleAboveTested.GetThirdCorner(), cv::Scalar(0, 255, 0), 2, 8);
	line(imgTestedClone, rectangleAboveTested.GetThirdCorner(), rectangleAboveTested.GetFirstCorner(), cv::Scalar(0, 255, 0), 2, 8);

	std::vector<std::vector<cv::Point>> contoursReference;
	std::vector<cv::Vec4i> hierarchyReference;
	cv::Mat cannyReference;

	cv::Mat contoursSourceReferenceImg = imgReferenceClone.clone();

	cvtColor(contoursSourceReferenceImg, contoursSourceReferenceImg, CV_BGR2GRAY);

	threshold(contoursSourceReferenceImg, contoursSourceReferenceImg, 67, 255, cv::THRESH_TOZERO);

	cv::Mat referenceGradX;
	Sobel(contoursSourceReferenceImg, referenceGradX, CV_16S, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
	cv::Mat absReferenceGradX;
	convertScaleAbs(referenceGradX, absReferenceGradX);

	cv::Mat referenceGradY;
	Sobel(contoursSourceReferenceImg, referenceGradY, CV_16S, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
	cv::Mat absReferenceGradY;
	convertScaleAbs(referenceGradY, absReferenceGradY);

	cv::Mat referenceSobelResult;
	addWeighted(absReferenceGradX, 0.5, absReferenceGradY, 0.5, 0, referenceSobelResult);

	GaussianBlur(referenceSobelResult, referenceSobelResult, cv::Size(11, 11), 0, 0, cv::BORDER_DEFAULT);

	Canny(referenceSobelResult, cannyReference, 75, 200);

	findContours(cannyReference, contoursReference, hierarchyReference, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	cv::Mat drawingReference = cv::Mat::zeros(referenceSobelResult.size(), CV_8UC3);

	std::sort(contoursReference.begin(), contoursReference.end(), compareContourAreas);
	
	std::vector<cv::Point> selectedVectorOfReferenceContours;

	for (int index = 0; index < contoursReference.size() && arcLength(contoursReference[index], false) > arcLength(contoursReference[0], false) * 0.01; index++)
	{
		selectedVectorOfReferenceContours.insert(selectedVectorOfReferenceContours.end(), contoursReference[index].begin(), contoursReference[index].end());
		drawContours(drawingReference, contoursReference, index, cv::Scalar(255, 255, 255), CV_FILLED, 8, hierarchyReference);
	}

	RectangleAboveContours rectangleAboveReference = RectangleAboveContours(selectedVectorOfReferenceContours, drawingReference);

	cv::Mat referenceImageSource = imgReferenceClone.clone();

	line(imgReferenceClone, rectangleAboveReference.GetFirstCorner(), rectangleAboveReference.GetSecondCorner(), cv::Scalar(0, 255, 0), 2, 8);
	line(imgReferenceClone, rectangleAboveReference.GetSecondCorner(), rectangleAboveReference.GetFourthCorner(), cv::Scalar(0, 255, 0), 2, 8);
	line(imgReferenceClone, rectangleAboveReference.GetFourthCorner(), rectangleAboveReference.GetThirdCorner(), cv::Scalar(0, 255, 0), 2, 8);
	line(imgReferenceClone, rectangleAboveReference.GetThirdCorner(), rectangleAboveReference.GetFirstCorner(), cv::Scalar(0, 255, 0), 2, 8);

	std::vector<cv::Point2f> refernceCorners;
	refernceCorners.push_back(rectangleAboveReference.GetFirstCorner());
	refernceCorners.push_back(rectangleAboveReference.GetSecondCorner());
	refernceCorners.push_back(rectangleAboveReference.GetFourthCorner());
	refernceCorners.push_back(rectangleAboveReference.GetThirdCorner());

	cv::Mat wrapedReference = ImageProcessorHelper().FourPointTransform(refernceCorners, referenceImageSource);

	if (wrapedReference.rows * wrapedReference.cols < wrapedTested.rows * wrapedTested.cols)
	{
		resize(wrapedTested, wrapedTested, cv::Size(wrapedReference.cols, wrapedReference.rows));
	}
	else
	{
		resize(wrapedReference, wrapedReference, cv::Size(wrapedTested.cols, wrapedTested.rows));
	}

	AnalyzedImages analizedImages;

	analizedImages.ReferenceImage = wrapedReference;
	analizedImages.TestedImage = wrapedTested;

	//Compare histograms
	HistogramsComparator histogramsComparator = HistogramsComparator(analizedImages.ReferenceImage.GetImage(), analizedImages.TestedImage.GetImage());

	cv::Mat imageWithSelectedAreas = histogramsComparator.FindDifferencesBetweenImages();

	/// Detect edges using canny
	std::vector<std::vector<cv::Point>> contoursForSelectedAreas;
	std::vector<cv::Vec4i> hierarchyForSelectedAreas;
	
	/// Find contours
	findContours(imageWithSelectedAreas, contoursForSelectedAreas, hierarchyForSelectedAreas, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	/// Draw contours

	for (int i = 0; i < contoursForSelectedAreas.size(); i++)
	{
		cv::Scalar color = cv::Scalar(0, 255, 0);
		drawContours(analizedImages.TestedImage.GetImage(), contoursForSelectedAreas, i, color, 2, 8, hierarchyForSelectedAreas, 0, cv::Point());
	}

	cv::Mat result = analizedImages.TestedImage.GetImage();

	/// Show in a window

	imshow("Reference image", imgReferenceClone);
	imshow("Tested image", imgTestedClone);

	imshow("Contours", result);
	
	cv::waitKey();
}