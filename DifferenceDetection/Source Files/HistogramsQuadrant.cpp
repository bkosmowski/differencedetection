#include "../Header Files/HistogramsQuadrant.h"

HistogramsQuadrant::HistogramsQuadrant() : firstQuarterImageHistogram(), secondQuarterImageHistogram(), thirdQuarterImageHistogram(), fourthQuarterHistogram()
{
}

HistogramsQuadrant::HistogramsQuadrant(cv::Mat inputImage)
{
	this->image = inputImage;
	CalculateHistogramForQuarters();
}

HistogramsQuadrant::~HistogramsQuadrant()
{
}

ImageHistogram HistogramsQuadrant::GetFirstQuarterHistoram() const
{
	return this->firstQuarterImageHistogram;
}

ImageHistogram HistogramsQuadrant::GetSecondQuarterHistogram() const
{
	return this->secondQuarterImageHistogram;
}

ImageHistogram HistogramsQuadrant::GetThirdQuarterHistogram() const
{
	return this->thirdQuarterImageHistogram;
}

ImageHistogram HistogramsQuadrant::GetFourthQuarterHistogram() const
{
	return this->fourthQuarterHistogram;
}

cv::Mat HistogramsQuadrant::GetImage() const
{
	return this->image;
}

void HistogramsQuadrant::CalculateHistogramForQuarters()
{
	cv::Rect firstQuarterArea = cv::Rect(0, 0, floor(this->image.cols / 2), floor(this->image.rows / 2));
	this->firstQuarterImage = this->image(firstQuarterArea);

	this->firstQuarterImageHistogram = ImageHistogram(firstQuarterImage);

	cv::Rect secondQuarterArea = cv::Rect(firstQuarterArea.width, 0, this->image.cols - firstQuarterArea.width, firstQuarterArea.height);
	this->secondQuarterImage = this->image(secondQuarterArea);

	this->secondQuarterImageHistogram = ImageHistogram(secondQuarterImage);

	cv::Rect thirdQuarterArea = cv::Rect(0, firstQuarterArea.height, firstQuarterArea.width, this->image.rows - firstQuarterArea.height);
	this->thirdQuarterImage = this->image(thirdQuarterArea);

	this->thirdQuarterImageHistogram = ImageHistogram(thirdQuarterImage);

	cv::Rect fourthQuarterArea = cv::Rect(firstQuarterArea.width, firstQuarterArea.height, this->image.cols - firstQuarterArea.width, this->image.rows - firstQuarterArea.height);
	this->fourthQuarterImage = this->image(fourthQuarterArea);

	this->fourthQuarterHistogram = ImageHistogram(fourthQuarterImage);
}
