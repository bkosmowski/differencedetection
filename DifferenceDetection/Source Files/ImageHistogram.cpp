#include "../Header Files/ImageHistogram.h"
#include "../Header Files/ImageProcessorHelper.h"

ImageHistogram::ImageHistogram()
{
}

ImageHistogram::ImageHistogram(cv::Mat inputImage)
{
	this->image = inputImage;
	
	this->lumaHistogram = ImageProcessorHelper::CalculateLumaHistogram(inputImage);

	this->hsvHistogram = ImageProcessorHelper::CalculateHSVHistogram(inputImage);
}

ImageHistogram::~ImageHistogram()
{
}

LumaHistogram ImageHistogram::GetLumaHistogram() const
{
	return this->lumaHistogram;
}


HSVHistogram ImageHistogram::GetHSVHistogram() const
{
	return this->hsvHistogram;
}

cv::Mat ImageHistogram::GetImage() const
{
	return this->image;
}

