#pragma once
#include "wtypes.h"
#include <stdio.h>
#include <cmath>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <opencv2/imgproc/imgproc.hpp>

#pragma comment(lib, "user32.lib")

#include "RectangleAboveContours.h"
#include "HistogramsQuadrant.h"
#include "HistogramsComparator.h"