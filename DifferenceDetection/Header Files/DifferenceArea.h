#pragma once
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <vector>

class DifferenceArea
{
public:
	DifferenceArea(std::vector<int> searchingPath, bool isDifference, cv::Size referenceImageSize, cv::Size testedImageSize);
	~DifferenceArea();
	cv::Mat CreateTestedImageWithSelectedArea(cv::Mat testedImage) const;
private:
	std::vector<int> searchingPath;
	bool isDifference;
	cv::Rect referenceRectangle;
	cv::Rect testedRectangle;

	cv::Rect CreateRectangleFromPath(cv::Size imageSize) const;
};
