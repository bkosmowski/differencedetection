#pragma once
#include "ImageHistogram.h"

struct AnalyzedImages
{
	ImageHistogram ReferenceImage;
	ImageHistogram TestedImage;
};
