#pragma once
#include "HistogramsQuadrant.h"
#include "ImageProcessorHelper.h"
#include "DifferenceArea.h"

class HistogramsComparator
{
public:
	HistogramsComparator(cv::Mat wrapedReference, cv::Mat wrapedTested);
	~HistogramsComparator();

	cv::Mat FindDifferencesBetweenImages();

private:
	int pathDepth;
	HistogramsQuadrant referenceHistogramQuadrant;
	HistogramsQuadrant testedHistogramQuadrant;

	cv::Mat ComposeImage(cv::Mat image, bool isDifferent, int elementIndex) const;
	std::vector<AnalyzedImages> DivideAnalyzedImages(AnalyzedImages analyzedImages) const;
	static int FindPathDepth(cv::Mat image);

	static double GetLumaDifference(ImageHistogram referenceHistogram, ImageHistogram testedHistogram);
	static double GetHueDifference(ImageHistogram referenceHistogram, ImageHistogram testedHistogram);
	static double GetSaturationDifference(ImageHistogram referenceHistogram, ImageHistogram testedHistogram);
};
