#pragma once
#include <opencv2/core/core.hpp>
#include "ImageHistogram.h"

class HistogramsQuadrant
{
public:
	HistogramsQuadrant();
	HistogramsQuadrant(cv::Mat inputImage);
	~HistogramsQuadrant();

	ImageHistogram GetFirstQuarterHistoram() const;
	ImageHistogram GetSecondQuarterHistogram() const;
	ImageHistogram GetThirdQuarterHistogram() const;
	ImageHistogram GetFourthQuarterHistogram() const;

	cv::Mat GetImage() const;

private:
	cv::Mat image;
	ImageHistogram firstQuarterImageHistogram;
	ImageHistogram secondQuarterImageHistogram;
	ImageHistogram thirdQuarterImageHistogram;
	ImageHistogram fourthQuarterHistogram;

	cv::Mat firstQuarterImage;
	cv::Mat secondQuarterImage;
	cv::Mat thirdQuarterImage;
	cv::Mat fourthQuarterImage;

	void CalculateHistogramForQuarters();
};
