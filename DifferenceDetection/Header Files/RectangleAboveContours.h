#pragma once
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "ImageProcessorHelper.h"

//
//  1st______2nd
//    |      |
//    |      |
//	  |		 |
//	3rd______4th
//

class RectangleAboveContours
{
public:

	RectangleAboveContours::RectangleAboveContours(std::vector<cv::Point> contours, cv::Mat contoursImage);
	~RectangleAboveContours();

	cv::Point2f GetFirstCorner() const;
	cv::Point2f GetSecondCorner() const;
	cv::Point2f GetThirdCorner() const;
	cv::Point2f GetFourthCorner() const;
	
	double GetAverageFirstX() const;
	double GetAverageSecondX() const;
	double GetAverageY() const;
	
private:
	cv::Point firstCorner;
	std::vector<cv::Point> potentialPointsForFirstCorner;

	cv::Point secondCorner;	
	std::vector<cv::Point> potentialPointsForSecondCorner;

	cv::Point thirdCorner;
	std::vector<cv::Point> potentialPointsForThirdCorner;

	cv::Point fourthCorner;
	std::vector<cv::Point> potentialPointsForFourthCorner;

	cv::RotatedRect minRect;
	double firstSideLength;
	double secondSideLength;
	double thirdSideLength;
	double fourthSideLength;

	double averageForY;
	double firstHalfAverageForX;
	double secondHalfAverageForX;
	cv::Mat contoursImage;
	cv::Point2f middle;


	static cv::Point2f GetMiddle(cv::Mat mat);

	static std::vector<cv::Point> GetFirstHalf(std::vector<cv::Point> contours, double averageForY);
	static std::vector<cv::Point> GetSecondHalf(std::vector<cv::Point> contours, double averageForY);

	static std::vector<cv::Point> GetFirstQuarter(std::vector<cv::Point> firstHalf, double averageForX);
	static std::vector<cv::Point> GetSecondQuarter(std::vector<cv::Point> firstHalf, double averageForX);
	static std::vector<cv::Point> GetSmallerPoints(std::vector<cv::Point> secondHalf, double averageForX);
	static std::vector<cv::Point> GetThirdQuarter(std::vector<cv::Point> secondHalf, double averageForX);
	static std::vector<cv::Point> GetLargerOrEqualPoints(std::vector<cv::Point> secondHalf, double averageForX);
	static std::vector<cv::Point> GetFourthQuarter(std::vector<cv::Point> secondHalf, double averageForX);
	static cv::Point GetMinXPoint(std::vector<cv::Point> points);
	static cv::Point GetMaxXPoint(std::vector<cv::Point> points);
	static cv::Point GetMinYPoint(std::vector<cv::Point> points);
	static cv::Point GetMaxYPoint(std::vector<cv::Point> points);

	static double GetAverageXFromContours(std::vector<cv::Point> contours);
	static double GetAverageYFromContours(std::vector<cv::Point> contours);

	double CalculateAngle(cv::Point firstPoint, cv::Point secondPoint, cv::Point thirdPoint) const;

	void SetPotentialsPointsForFirstCorner(std::vector<cv::Point> contours);
	void SetPotentialsPointForSecondCorner(std::vector<cv::Point> contours, int matWidth);
	void SetPotentialsPointForThirdCorner(std::vector<cv::Point> contours, int matHeight);
	void SetPotentialPointsForFourthCorner(std::vector<cv::Point> contours, int matWidth, int matHeight);

	void SetPotentialCorners(cv::Point firstPotentialCorner, cv::Point secondPotentialCorder, cv::Point thirdPotentialCorner, cv::Point fourthPotentialCorner);
	void SetCornersForMaxArea();
	double CaulculateArea(cv::Point firstPoint, cv::Point secondPoint, cv::Point thirdPoint, cv::Point fourthPoint) const;
};