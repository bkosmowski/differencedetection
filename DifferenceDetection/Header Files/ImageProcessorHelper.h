#pragma once
#include "AnalyzedImages.h"
#include "HSVHistogram.h"
#include <opencv2/imgproc/imgproc.hpp>

class ImageProcessorHelper
{
public:
	ImageProcessorHelper();
	~ImageProcessorHelper();

	static double CompareHueHistograms(HSVHistogram referenceHistogram, HSVHistogram testedHistogram);
	static double CompareSaturationHistograms(HSVHistogram referenceHistogram, HSVHistogram testedHistogram);
	static double CompareLumaHistograms(LumaHistogram referenceHistogram, LumaHistogram testedHistogram);

	static LumaHistogram CalculateLumaHistogram(cv::Mat image);

	static double CalculateEuclideanDistance(cv::Point firstPoint, cv::Point secondPoint);
	static double CalculateLuma(int blue, int green, int red);
	static HSVHistogram CalculateHSVHistogram(cv::Mat image);

	static cv::Mat FourPointTransform(std::vector<cv::Point2f> corners, cv::Mat sourceImage);

	static int GetBucketForLuma(double luma);
};
