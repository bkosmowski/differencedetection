#pragma once
#include <opencv2/core/core.hpp>
#include "LumaHistogram.h"
#include "HSVHistogram.h"

class ImageHistogram
{
public:
	ImageHistogram();
	ImageHistogram(cv::Mat inputImage);
	~ImageHistogram();

	LumaHistogram GetLumaHistogram() const;
	HSVHistogram GetHSVHistogram() const;
	cv::Mat GetImage() const;

private:
	LumaHistogram lumaHistogram;
	cv::Mat image;
	HSVHistogram hsvHistogram;
};
